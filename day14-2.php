<?php
// advent of code 2020 14/2
$input_array=array_map('trim',file('day14.data'));

$max_floating_bits_in_line=0;
$working_mem=array();

foreach($input_array as $input_line)
 {
  list($left_part,$right_part)=array_map('trim',explode ('=',$input_line));
  if ($left_part=='mask')
   {
    $input_bits=$right_part;// string 01X01X
    $float_bits=array();
    $or_mask=0;
    $cnt_float_bits=0;
    for($i=0; $i<strlen($input_bits); $i++)
     {
      $shift_amount=strlen($input_bits)-$i-1;
      switch($input_bits[$i])
       {
        case 'X':
        $float_bits[]= $shift_amount;
        $cnt_float_bits++;
        break;
        case '0':
        break;
        case '1':
        $or_mask |= 1<<$shift_amount;
        break;
        default: echo "bad character [$input_bits] i=$i\n";
       }
     }
   }
  else
   {
    $data_to_write=$right_part;
    if (strtok($left_part,'[] ') != 'mem') echo "sumfin wrung\n"; // skip the 'mem'
    $addr=strtok('[] ');
    $float_size= 1<<$cnt_float_bits;
    $or_masked_addr=$addr | $or_mask;
    for($j=0; $j<$float_size; $j++) // for each fluctuation, 2^fluctuation_count
     {
      $addr_write=$or_masked_addr; // init address for next fluctuation
      for($i=0; $i<$cnt_float_bits; $i++) // set fluctuating bits one by one
       {
        if (($j >> $i) & 1)  $addr_write |= 1<<$float_bits[$i]; // set 1
        else $addr_write &= ~(1<<$float_bits[$i]); // set 0
       }
      $working_mem[$addr_write]=$data_to_write;
     }
    $max_floating_bits_in_line=max($cnt_float_bits, $max_floating_bits_in_line);
   }
 }
$sum=0;
foreach($working_mem as $m) $sum+=$m;
echo "memory addresses used=".count($working_mem)."\n";
echo "sum=$sum\n";
echo "max floating bits in line=$max_floating_bits_in_line\n";
