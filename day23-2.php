<?php
// advent of code 2020 23/1
ini_set('memory_limit','512M');

//$size=9;
//$cycles=100;
//$data_example='389125467';

$size=1000000;
$cycles=10000000;
$data='418976235';

$s=str_split($data);

foreach($s as $n => $nr)
 {
  $labels[]=$nr;
  $links[]=$n+1;
 }

$search=array_flip($s); // values on cup. key=>value reversed for quick search

if ($size>9)
for($i=10; $i<=$size; $i++)
 {
  $labels[]=$i; // values on cup
  $links[]=$i;   // links to next cup
  $search[$i]=$i-1;
 }

$links[count($links)-1]=0; // last cup links to 0

function crab_decrement(int $i)
 {
  global $size;
  $j=$i-1;
  if ($j>0) return $j;
  return $size;
 }



function nxt(int $pos)
 {
  global $links;
  return intval($links[$pos]);
 }

function setnxt(int $pos1,int $pos2)
 {
  global $links;
  $links[$pos1]=nxt($pos2);
 }

function val(int $pos)
 {
  global $labels;
  return intval($labels[$pos]);
 }

$read_pos=0;
echo "\n";

for($i=1; $i<=$cycles; $i++)
 {
  $pos1=nxt($read_pos);
  $val1=val($pos1);
  $pos2=nxt($pos1);
  $val2=val($pos2);
  $pos3=nxt($pos2);
  $val3=val($pos3);

  $selected3=array($val1,$val2,$val3);

  $dest=val($read_pos);
  do
   {
    $dest=crab_decrement($dest);
   }
  while(in_array($dest,$selected3));

  $dest_pos=$search[$dest];

  setnxt($read_pos,$pos3);
  setnxt($pos3,$dest_pos);
  $links[$dest_pos]= $pos1;

  $read_pos=nxt($read_pos);
 }

echo "\n";
$result_pos=array_search(1,$labels);
if ($result_pos===false) echo "not found 1 \n";

$mul=1;
for($i=0; $i<3; $i++)
 {
  echo $labels[$result_pos]."\n";
  $mul*=$labels[$result_pos];
  $result_pos=$links[$result_pos];
 }
echo " result=$mul\n";

