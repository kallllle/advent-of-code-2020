<?php
//  advent of code 2020 1/1

$f=array_map('intval',file('day1.data'));



echo " solution v1\n";
foreach ($f as $n1)
foreach($f as $n2)
if ( $n1+$n2 == 2020 )
 {
  echo "$n1 $n2 ".$n1*$n2."\n";
  break 2;
 }


echo " solution v1.1\n";
function chk2($a,$b)
 {
  return ($a+$b==2020);
 }
foreach ($f as $n1)
foreach($f as $n2)
if (chk2 ( $n1,$n2 ) )
 {
  echo "$n1 $n2 ".$n1 * $n2."\n";
  break 2;
 }


echo " solution v1.2\n";
function chk($a,$b)
 {
  if ($a+$b==2020) echo "$a $b ".$a*$b."\n";
 }
foreach ($f as $n1)
foreach($f as $n2)
chk ( $n1,$n2 );


echo " solution v2\n";
function map2($fn, $array)
 {
  foreach($array as $n1=>$a1)
   {
    foreach($array as $n2=>$a2) if ($n1 > $n2)
     {
      $fn($a1,$a2);
     }
   }
 }
map2( function($a,$b) {if ($a+$b==2020) echo "$a $b ".$a*$b."\n"; }, $f);



echo " solution v2.1\n";
function map2a($fn, $array)
 {
  foreach($array as $n1=>$a1)
   {
    foreach($array as $n2=>$a2) if ($n1>$n2)
     {
      if ($fn($a1,$a2)) break 2;
     }
   }
 }
function chk3($a,$b)
 {
  if ($a+$b==2020)
   {
    echo "$a $b ".$a*$b."\n";
    return true;
   }
 }
map2a('chk3',$f);

