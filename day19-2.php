<?php
// advent of code 2020 19/2

$input_data=amtf('day19.data');

//$rulez_8_11=amtf('day19-rule-8-11.data');
$rulez_8_11=array('8: 42 | 42 8',
'11: 42 31 | 42 11 31');

function trim2($s)
 {
  return str_replace('"','',trim($s));
 }

function amte($str,$array)
 {
  return array_map('trim',explode($str,$array));
 }

function amtf($filename)
 {
  return array_map('trim',file($filename));
 }


$file_section_cnt=0;
foreach($input_data as $line)
 {
  if (empty($line)) $file_section_cnt++;
  else
   {
    if (!$file_section_cnt)
     {
      list($nr,$rules_part)=explode(':',$line);
      $rz=amte('|',$rules_part);
      $rrz=array();
      foreach($rz as $r) $rrz[]=array_map('trim2',explode(' ',$r));
      $rulez[$nr]=$rrz;
     }
    else
    $msgs[]=$line;
   }
 }

foreach($rulez_8_11 as $rule)
 {
  list($nr,$rules_part)=explode(':',$rule);
  $rz=amte('|',$rules_part);
  $rrz=array();
  foreach($rz as $r) $rrz[]=array_map('trim2',explode(' ',$r));
  $rulez[$nr]=$rrz;
 }


function combine($arr1,$arr2,$rule_nr) // 
 {
  global $msglen,$message_str,$ready;
  $res=array();
  if (count($arr1)==0 || count($arr2)==0 ) return $res;

  foreach($arr1 as $a1)
   {
    foreach($arr2 as $a2)
     {
      $s=$a1.$a2;
      if ($s==$message_str && $rule_nr==0) $ready=1; // found
      if ((strlen($s)<=$msglen)  // dont generate strings larger than message
       && (strpos($message_str,$s)!==false)) // does message contain this substring ?
         $res[]=$s;
     }
   }
  return $res;
 }


function genmatches($nr,$rlevel)
 {
  global $rulez,$message_str,$msglen,$matches;

  if ($rlevel<1) return array();  // stop recursion
  if(isset($matches[$nr])) return $matches[$nr]; // result from cache
  $r=$rulez[$nr];
  $ret=array();
  if (is_array($r))
   {
    foreach($r as $r_nr=>$rul) // (2 3 | 3 2) as (2 3)
     {
      if (is_array($rul))
       {
        switch (count($rul))
         {
          case 1:
          if (!is_numeric($rul[0])) $comb=array($rul[0]); // a b
          else $comb=genmatches($rul[0],$rlevel-1);
          break;

          case 2:
          $g0=(is_numeric($rul[0])) ? genmatches($rul[0],$rlevel-1) : array($rul[0]);
          $g1=(is_numeric($rul[1])) ? genmatches($rul[1],$rlevel-1) : array($rul[1]);
          $comb=combine($g0, $g1,$nr);
          break;

          default:
          $g0=(is_numeric($rul[0])) ? genmatches($rul[0],$rlevel-1) : array($rul[0]);
          $r1=$rul[1];

          if ($rul[0]==$rul[1]) $comb=combine($g0, $g0,$nr);
          else
           {
            $g1=(is_numeric($rul[1])) ? genmatches($rul[1],$rlevel-1) : array($rul[1]) ;
            $comb=combine($g0,$g1, $nr);
           }
          foreach($rul as $rn=>$rr) if ($rn>1)
           {
            $g2=(is_numeric($rr)) ? genmatches($rr,$rlevel-1) : array($rr);
            $comb=combine($comb,$g2, $nr);
           }
         }
       }
      $ret=array_merge($ret,$comb);
     }
   }
  else echo "not array [$r]\n";
  $matches[$nr]=$ret; // results to cache
  return $ret;
 }

$ccnt=0;
foreach($msgs as $m)
 {
  echo "msg=$m len=".strlen($m);
  $message_str=$m;
  $msglen=strlen($m);
  $ready=0;
  $matches=array();
  genmatches(0,25);
  if ($ready==1) {$ccnt++;echo '  +';}
  echo "\n";
 }

echo "matches=$ccnt\n";

